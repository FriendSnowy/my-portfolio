<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'portfolio');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pm^cl.cJ<*y-YBX:bvFNSZ&|LMM_OzDRV0}{kQ|l7pG5,`h8=]!af]G$E5WvE{fY');
define('SECURE_AUTH_KEY',  'C$89/jzelSoEe~v0=lax%|YqZe1x:)yuI#SH/Wp7u%|7>?ozp33C8 &x.XVCGUF{');
define('LOGGED_IN_KEY',    ' l%y-W[dNi%ww,sJ>1P.V$fvt1zspwH df5$%e&$I2}vBZ.X>8i_4* (WfuSo=IP');
define('NONCE_KEY',        '%:M6G)m`]LF^gLMh!]<^k>J.eM8|7Je(zemYvZ|~wdr`pG^qi0hEF:k#R(uspo?6');
define('AUTH_SALT',        ' fj~y2QPaa%2P9?EDi^gXB5XdA1iR8F#JPBwXdYDpnq4qF4QfEWL%3B6L1FTCfE.');
define('SECURE_AUTH_SALT', 'p%wN909%+No{lI(x&YkcgJoka+19uL_zPVO+W]/7tGBi;%ufQfO4dlVD$,.jjOEF');
define('LOGGED_IN_SALT',   '-JKF{uesIrY?%@ptx<n+C#X*-!84YP|r,Q{Xbv-:=YL~>Gu`O?Y1ZIUlf#O`[[#:');
define('NONCE_SALT',       'V_ND?Iiq.?OGGZz%B=0b0Hh!W*+xxU}E?Xmo9!935=Pgv5LJi|8Y[]t`&ABeX4bn');
define('WP_HOME','http://portfoilo.loc');
define('WP_SITEURL','http://portfoilo.loc');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
