<?php

function createOrder() {
    $post_data = array(
        'post_title' => wp_strip_all_tags($_POST['name']),
        'post_type' => 'orders'
    );

    $id = wp_insert_post($post_data, true);
    if ($id == 0)
        echo 'err';
    else {
        echo $_POST['phone'];
        update_field("user_name", $_POST['name'], $id);
        update_field("user_mail", $_POST['mail'], $id);
        update_field("user_phone", $_POST['phone'], $id);
    }
    die();
}

add_action('wp_ajax_postview_createOrder', 'createOrder');
add_action('wp_ajax_nopriv_createOrder', 'createOrder');

