<?php
/* PostType Page Template: Проект */
?>
<!DOCTYPE html>
<html lang="ru">
<?php get_header(); ?>
    <header class = "header__project-header" style = "background-color: <?= get_field('project_header_color'); ?>; background-image: url(<?= get_field('project_header_img'); ?>)"></header>
    <div class = "size-normal content">
        <div class = "size-full container">
            <div class = "block clearfix">
                <div class="title-project"><H2>О проекте</H2></div>
                <div class="content">
                    <div class = "text-project">
                        <?php echo get_field("project_text_about"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class = "size-full project-slider" style = "background-color: <?= get_field('project_header_color'); ?>">
<?php   $background = get_template_directory_uri(); 
        if (get_field('project_slider_color') == "white")
            $background = $background . '/img/arrow_light.png';
        else 
            $background = $background . '/img/arrow.png'; ?>
        <div class = "project-slider__controller" id="project-slider__back" style = "background-image: url(<?= $background; ?>)"></div>
        <div id = "project-slider__image_wrap">
<?php   if (have_rows('project__slider')):
            $i = 0;
            while (have_rows('project__slider')) : the_row();
                if (get_sub_field('project_image')):  ?>
                    <img src = "<?php echo get_sub_field('project_image'); ?>" style = "<?php if ($i!=0) echo "display: none";?>" class = "<?php if ($i==0) echo "active"; ?>" id = "img_<?=$i;?>"/>
<?php               $i++;
                endif;
            endwhile;
        endif; ?>
        </div>
        <div class = "project-slider__controller" id="project-slider__forward" style = "background-image: url(<?= $background; ?>)"></div>
    </div>
<?php if (have_rows('project_tech')) : ?>
    <div class = "size-normal content">
        <div class = "size-full container">
            <div class = "block clearfix">
                <div class="title-project"><H2>Используемые технологии</H2></div>
                <div class="project__tech-wrapper">
<?php               while (have_rows('project_tech')) : the_row(); 
                        $id = get_sub_field('selected_tech'); ?>
                        <div class="project__tech-slot"style = "background-image: url(<?= get_field('tech_image', $id); ?>); background-color: <?= get_field('tech_color', $id); ?>;">
                            <div class="description">
                                <?= get_field('tech_description', $id); ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php get_footer(); ?>
<script>
    
    let sliderImages = $("#project-slider__image_wrap").children();
    let techs = $(".project__tech-slot");
    
    let moveImage = function(currentImgID, nextImgID) {
            $("#img_"+currentImgID).removeClass("active");
            $("#img_"+nextImgID).addClass("active");
            $("#img_"+currentImgID).hide(200);
            $("#img_"+nextImgID).show(200);
    }
    
    $("#project-slider__back").on('click', function() {
        if (sliderImages.length > 1) {
            for (let i = 0; i < sliderImages.length; i++){
                if ($("#img_"+i).hasClass("active")) {
                    if (i == 0)  nextImg = sliderImages.length - 1;
                    else nextImg = i-1;
                    moveImage(i, nextImg);
                    break;
                }
            }
        }
    })
    
    $("#project-slider__forward").on('click', function() {
        if (sliderImages.length > 1) {
            for (let i = 0; i < sliderImages.length; i++){ 
                if ($("#img_"+i).hasClass("active")) {
                    if (i == sliderImages.length - 1) nextImg = 0;
                    else nextImg = i+1;
                    moveImage(i, nextImg);
                    break;
                }
            }
        }
    })
</script>
</html>
