<?php
    /*Template Name: Главная */
?>
<!DOCTYPE html>
<html lang="ru">
<?= get_header(); ?>
<body>
    <header class = "size-full header__main-header"> </header>
    <div class = "size-normal content">
        <div class = "size-full container">
            <div class = "block clearfix">
                <div class="title"><H2>Обо мне</H2></div>
                <div class="content">
                    <div class = "photo">
                        <img src = "<?php echo get_field("about_me_img"); ?>"/>
                    </div>
                    <div class = "text">
                        <?php echo get_field("about_me_txt"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class = "size-full projects__projects-wrap">
        <div class="size-normal content clearfix">

<?php       
    $projects = get_posts(
        array (
            'post_type' => 'project'
        )
    );

    for ($i = 0; $i <= count($projects)%3; $i++) {
?>
    <div class = "projects__project-row">
        <?php
            for ($j = 0 + $i*3; $j < 3 + $i*3; $j++) {
                if (empty($projects[$j])) continue; 
        ?>
            <div class="projects__project" style="background-color: <?= get_field('project_header_color', $projects[$j]->ID); ?>">
                <div class = "size-full projects__project-info-wrap">
                    <img src = "<?php echo get_field('project_anounce_img', $projects[$j]->ID); ?>"/>
                    <button>Подробнее</button>
                </div>
                <div class = "projects__project-about">
                    <H3><?php echo $projects[$j]->post_title ?></H3>
                    <p><?php echo get_field('project_anounce', $projects[$j]->ID); ?></p>
                    <div class = "projects__project-button-wrap">
                        <a href = "<?php echo get_permalink($projects[$j]); ?>" class = "projects__project-button">Подробнее</a>
                    </div>
                </div>
            </div>
        <?php
            }
        ?>
    </div>
<?php } ?>
        </div>
    </div>
    <div class = "size-normal content mobile-hide">
        <div class = "size-full container">
            <div class = "block clearfix">
                <div class="title"><H2>Преимущества</H2></div>
                <div class = "slider__slider-wrapper">
                    <div id = "slider__slider-controller-left" class = "slider__slider-controller"></div>
                    <div id = "slider__slider-image">
 <?php              if (have_rows('advantage_slider')):
                        $i = 0;
                         while (have_rows('advantage_slider')) : the_row();
                            if (get_sub_field('image')):  ?>
                                <img src = "<?php echo get_sub_field('image'); ?>" style = "<?php if ($i==0): echo ""; else: echo "display: none"; endif;?>" />
<?php                           $i++;
                            endif;
                        endwhile;
                    endif; ?>
                    </div>
                    <div id="slider__slider-description">
<?php               if (have_rows('advantage_slider')):
                        $i = 0;
                        while (have_rows('advantage_slider')) : the_row();
                            if (get_sub_field('image')): ?>
                                <div class = "slider__description-block" style = "<?php if ($i == 0): echo ""; else: echo "display: none"; endif; ?>">
                                    <div class="title">
                                        <H3><?= get_sub_field('title'); ?></H3>
                                    </div>
                                    <div class="text">
                                        <?= get_sub_field('description'); ?>
                                    </div>
                                </div>
<?php                           $i++;
                            endif;
                        endwhile;
                    endif; ?>
                    </div>
                    <div id = "slider__slider-controller-right" class = "slider__slider-controller"></div>
                </div>
            </div>
        </div>
    </div>
<?php
    $projects = get_posts(array('post_type'=>'project'));
    $usedTechs = array();
    $techsCount = array();
    foreach ($projects as $project) {
        $techs = get_field('project_tech', $project->ID);
        foreach ($techs as $tech)
            array_push($usedTechs, $tech['selected_tech']);
    }

    foreach ($usedTechs as $usedTech) {
        if (!array_key_exists($usedTech, $techsCount))
            $techsCount[$usedTech] = 1;
        else
            $techsCount[$usedTech] += 1;
    }
    asort($techsCount);
?>
    <div class="size-full skills__skills-wrap">
        <div class="size-normal content clearfix">
            <div class="title"><H2>Используемые технологии</H2></div>
            <div class="skills__skills-block-wrap">
<?php       if (count($techsCount > 0)):
            $projectCount = count($projects);
                foreach ($techsCount as $id=>$tech):?>
                    <div class = "skills__skill-block">
                        <div class = "skills__skill-row">
                            <h3> <?= get_the_title($id); ?></h3>
                            <p> <?= $tech ?> / <?= $projectCount; ?> проектов</p>
                        </div>
                        <div class = "skills__skill-line" style = "width: <?php echo ($tech / $projectCount) * 100 ; ?>%; max-width: 0%;"></div>
                    </div>
<?php           endforeach;
            endif; ?>
            </div>
        </div>
    </div>

    <div class = "size-normal content">
        <div class = "size-full container">
            <div class = "block clearfix">
                <div class="title"><H2>Заказать разработку</H2></div>
                <div class="content" id = "form__form-content">
                    <div class="form__form-text"><?= get_field("form-text"); ?></div>
                    <div class="form__form_content">
                        <form>
                            <input id = "form__name-input" type = "text" placeholder="Ваше имя"/>
                            <input id = "form__email-input" type = "text" placeholder="Ваш E-mail"/>
                            <input id = "form__phone-input" type = "text" placeholder="Ваш телефон"/>
                            <input id = "form__form-submit" type = "submit"/>
                        </form>
                    </div>
                </div>
                <div class="content" id="form__form-success">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/checked.png" alt="Принято!" class="form__form-success-img">
                    <div class="form__form-success-content">
                        <p class="form__form-success-text">Заявка отправлена! Скоро я свяжусь с вами для уточнения информации.</p>
                        <div class="form__form-success-repeat">Хочу еще</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php get_footer(); ?>
</body>
<script>
    let skillLines = $(".skills__skill-line");
    let projects = $(".projects__project");

    function moveSliderElementForward(sliderElements) {
        for (let i = 0; i < sliderElements.length; i++){
            if (sliderElements.item(i).getAttribute("style") == "") {
                sliderElements.item(i).setAttribute("style", "display: none");
                if ((i+1) >= sliderElements.length)
                    i = 0;
                else i++;
                sliderElements.item(i).setAttribute("style","");
                break;
            }
        }
    }

    function moveSliderElementBackward(sliderElements) {
        for (let i = sliderElements.length - 1; i >= 0; i--){
            if (sliderElements.item(i).getAttribute("style") == "") {
                sliderElements.item(i).setAttribute("style", "display: none");
                if ((i-1) < 0)
                    i = sliderElements.length - 1;
                else i--;
                sliderElements.item(i).setAttribute("style","");
                break;
            }
        }
    }

    function moveSliderForward() {
        let sliderElements = document.getElementById('slider__slider-image').children;
        moveSliderElementForward(sliderElements);

        sliderElements = document.getElementById('slider__slider-description').children;
        moveSliderElementForward(sliderElements);
    }

    function moveSliderBackward() {
        let sliderElements = document.getElementById('slider__slider-image').children;
        moveSliderElementBackward(sliderElements);

        sliderElements = document.getElementById('slider__slider-description').children;
        moveSliderElementBackward(sliderElements);
    }

    $(document).ready(function(){
        let arr = $(".projects__project");

        $.each(arr, function() {
            $(this).on("mouseenter", function() {
                $(this).find(".projects__project-info-wrap").fadeOut(200);
                $(this).find(".projects__project-about").show();
            });
            $(this).on("mouseleave", function() {
                $(this).find(".projects__project-info-wrap").fadeIn(200);
                $(this).find(".projects__project-about").hide();
            })
        });

        var sliderController = document.getElementById('slider__slider-controller-right');
        sliderController.addEventListener("click", moveSliderForward);

        sliderController = document.getElementById('slider__slider-controller-left');
        sliderController.addEventListener("click", moveSliderBackward);

        $(".form__form-success-repeat").click(function() {
            $("#form__form-content").show();
            $("#form__form-success").hide();
        });

        $("#form__form-submit").click(function() {
            event.preventDefault();
            let validated = true;
            let nameInput = $("#form__name-input");
            let emailInput = $("#form__email-input");
            let phoneInput = $("#form__phone-input");
            if (nameInput.val() == "") {
                nameInput.addClass("notok").attr({placeholder: "Поле не должно быть пустым"}).val("");
                validated = false;
            }
            else
                nameInput.removeClass("notok").attr({placeholder: "Ваше имя"});

            if (!emailInput.val().match(/[A-Za-z]*@[A-Za-z]*/)) {
                emailInput.addClass("notok").attr({placeholder: "Некорректный E-mail"}).val("");
                validated = false;
            }
            else
                emailInput.removeClass("notok").attr({placeholder: "Ваш E-mail"});

            if (!phoneInput.val().match(/[0-9-+]/)) {
                phoneInput.addClass("notok").attr({placeholder: "Допускаются только цифры и знак -"}).val("");
                validated = false;
            }
            else
                phoneInput.removeClass("notok").attr({placeholder: "Ваш телефон"});

            if (validated) {
                $.ajax({
                    url: "/wp-admin/admin-ajax.php",
                    type: "POST",
                    data: "action=createOrder&name=" + nameInput.val() + "&mail=" + emailInput.val() + "&phone=" + phoneInput.val(),
                    success: function(data){
                        if (data == 'err')
                            alert('Ошибка');
                        else {
                            $("#form__form-content").hide();
                            $("#form__form-success").show();
                        }
                    }
                });
            }
            else return false;
        })
    });

    $(document).scroll(function() {
        $.each(skillLines, function(){
            if ($(this).visible() && !$(this).hasClass("wide")) {
                $(this).addClass("wide");
                $(this).animate({
                    'max-width' : '100%'
                }, 'slow');
            }
        });
    });
</script>
</html>
