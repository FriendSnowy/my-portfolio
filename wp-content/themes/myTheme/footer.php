<?php 
$projects = get_posts(
    array (
        'post_type' => 'project'
    )
);
?>
<footer class = "size-full footer__main-footer">
    <div class="size-normal content">
        <div class="footer__content-block">
            <div class="footer__content-block-title">
                Последние проекты
            </div>
<?php       for($i = 0; $i < (count($projects) > 3 ? 3 : count($projects)); $i++): ?>
            <div class="footer__content-block-link">
                <a href = "<?=get_permalink($projects[$i]->ID);?>"><?=$projects[$i]->post_title;?></a>
            </div>
<?php       endfor; ?>
        </div>
        <div class="footer__content-block">
            <div class="footer__content-block-title">
                Автор
            </div>
            <div class="footer__content-block-link">
                Гафиатуллин Альберт
            </div>
            <div class="footer__content-block-link">
                friendsnowy@gmail.com
            </div>
            <div class="footer__content-block-link">
                г. Екатеринбург, 2017 г.
            </div>
        </div>
    </div>
</footer>
</body>