<head>
    <meta charset="UTF-8"/>
    <title><?= get_the_title(); ?></title>
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/scripts/jquery.visible.min.js"></script>
</head>
<body>
<?php if (!is_front_page()):?>
    <a class="go_back" href = "<?= get_home_url();?>"></a>
<?php endif;?>