<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8"/>
    <title>Document</title>
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/scripts/jquery.visible.min.js"></script>
</head>
<body>
    <header class = "size-full header__main-header"> </header>
    <div class = "size-normal content">
        <div class = "size-full container">
            <div class = "block clearfix">
                <div class="title"><H2>Обо мне</H2></div>
                <div class="content">
                    <div class = "photo">
                        <img src = "<?php echo get_field("about_me_img"); ?>"/>
                    </div>
                    <div class = "text">
                        <?php echo get_field("about_me_txt"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class = "size-full projects__projects-wrap">
        <div class="size-normal content clearfix">
            <div class="projects__project-row">
<?php       $projects = get_posts(
                array (
                    'post_type' => 'project'
                )
            );

            foreach($projects as $project) { ?>
                <div class="projects__project">
                    <div class = "size-full projects__project-info-wrap">
                        <img src = "<?php echo get_field('project_anounce_img', $project->ID); ?>"/>
                        <button>Подробнее</button>
                    </div>
                    <div class = "projects__project-about">
                        <H3><?php echo $project->post_title ?></H3>
                        <p><?php echo get_field('project_anounce', $project->ID); ?></p>
                        <div class = "projects__project-button-wrap">
                            <a href = "<?php echo get_permalink($project); ?>" class = "projects__project-button">Подробнее</a>
                        </div>
                    </div>
                </div>
<?php       }?>
            </div>
        </div>
    </div>
    <div class = "size-normal content">
        <div class = "size-full container">
            <div class = "block clearfix">
                <div class="title"><H2>Преимущества</H2></div>
                <div class = "slider__slider-wrapper">
                    <div id = "slider__slider-controller-left" class = "slider__slider-controller"></div>
                    <div id = "slider__slider-image">
 <?php              if (have_rows('advantage_slider')):
                        $i = 0;
                         while (have_rows('advantage_slider')) : the_row();
                            if (get_sub_field('image')):  ?>
                                <img src = "<?php echo get_sub_field('image'); ?>" style = "<?php if ($i==0): echo ""; else: echo "display: none"; endif;?>" />
<?php                           $i++;
                            endif;
                        endwhile;
                    endif; ?>
                    </div>
                    <div id="slider__slider-description">
<?php               if (have_rows('advantage_slider')):
                        $i = 0;
                        while (have_rows('advantage_slider')) : the_row();
                            if (get_sub_field('image')): ?>
                                <div class = "slider__description-block" style = "<?php if ($i == 0): echo ""; else: echo "display: none"; endif; ?>">
                                    <div class="title">
                                        <H3><?= get_sub_field('title'); ?></H3>
                                    </div>
                                    <div class="text">
                                        <?= get_sub_field('description'); ?>
                                    </div>
                                </div>
<?php                           $i++;
                            endif;
                        endwhile;
                    endif; ?>
                    </div>
                    <div id = "slider__slider-controller-right" class = "slider__slider-controller"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="size-full skills__skills-wrap">
        <div class="size-normal content clearfix">
            <div class="title"><H2>Навыки</H2></div>
            <div class="skills__skills-block-wrap">
<?php       if (have_rows('skills')):
                while (have_rows('skills')) : the_row(); ?>
                    <div class = "skills__skill-block">
                        <div class = "skills__skill-row">
                            <h3> <?php echo get_sub_field('skill'); ?></h3>
                            <p> <?php echo get_sub_field('level'); ?> / 10</p>
                        </div>
                        <div class = "skills__skill-line" style = "width: <?php echo get_sub_field('level') * 10; ?>%; max-width: 0%;"></div>
                    </div>
<?php           endwhile;
            endif; ?>
            </div>
        </div>
    </div>

    <div class = "size-normal content">
        <div class = "size-full container">
            <div class = "block clearfix">
                <div class="title"><H2>Заказать разработку</H2></div>
                <div class="content">
                    <div class="form__form-text"><?= get_field("form-text"); ?></div>
                    <div class="form__form_content">
                        <form>
                            <input id = "form__name-input" type = "text" placeholder="Ваше имя"/>
                            <input id = "form__email-input" type = "text" placeholder="Ваш E-mail"/>
                            <input id = "form__form-submit" type = "submit"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php get_footer(); ?>
</body>
<script>
    function moveSliderElementForward(sliderElements) {
        for (let i = 0; i < sliderElements.length; i++){
            if (sliderElements.item(i).getAttribute("style") == "") {
                sliderElements.item(i).setAttribute("style", "display: none");
                if ((i+1) >= sliderElements.length)
                    i = 0;
                else i++;
                sliderElements.item(i).setAttribute("style","");
                break;
            }
        }
    }

    function moveSliderElementBackward(sliderElements) {
        for (let i = sliderElements.length - 1; i >= 0; i--){
            if (sliderElements.item(i).getAttribute("style") == "") {
                sliderElements.item(i).setAttribute("style", "display: none");
                if ((i-1) < 0)
                    i = sliderElements.length - 1;
                else i--;
                sliderElements.item(i).setAttribute("style","");
                break;
            }
        }
    }

    function moveSliderForward() {
        let sliderElements = document.getElementById('slider__slider-image').children;
        moveSliderElementForward(sliderElements);

        sliderElements = document.getElementById('slider__slider-description').children;
        moveSliderElementForward(sliderElements);
    }

    function moveSliderBackward() {
        let sliderElements = document.getElementById('slider__slider-image').children;
        moveSliderElementBackward(sliderElements);

        sliderElements = document.getElementById('slider__slider-description').children;
        moveSliderElementBackward(sliderElements);
    }

    $(document).ready(function(){
        let arr = $(".projects__project");

        $.each(arr, function() {
            $(this).on("mouseenter", function() {
                $(this).find(".projects__project-info-wrap").fadeOut(200);
                $(this).find(".projects__project-about").show();
            })
            $(this).on("mouseleave", function() {
                $(this).find(".projects__project-info-wrap").fadeIn(200);
                $(this).find(".projects__project-about").hide();
            })
        });

        var sliderController = null;

        sliderController = document.getElementById('slider__slider-controller-right');
        sliderController.addEventListener("click", moveSliderForward);

        sliderController = document.getElementById('slider__slider-controller-left');
        sliderController.addEventListener("click", moveSliderBackward);

        $("#form__form-submit").click(function() {
            event.preventDefault();
            let validated = true;
            if ($("#form__name-input").val() == "") {
                $("#form__name-input").addClass("notok").attr({placeholder: "Поле не должно быть пустым"}).val("");
                validated = false;
            }
            else {
                $("#form__name-input").removeClass("notok").attr({placeholder: "Ваше имя"});
            }

            if (!$("#form__email-input").val().match(/[A-Za-z]*@[A-Za-z]*/)) {
                $("#form__email-input").addClass("notok").attr({placeholder: "Некорректный E-mail"}).val("");
                validated = false;
            }
            else {
                $("#form__email-input").removeClass("notok").attr({placeholder: "Ваш E-mail"});
            }

            if (validated) return true;
            else return false;
        })
    });

    $(document).scroll(function() {
        let skillLines = $(".skills__skill-line");
        let projects = $(".projects__project");
        $.each(skillLines, function(){
            if ($(this).visible() && !$(this).hasClass("wide")) {
                $(this).addClass("wide");
                $(this).animate({
                    'max-width' : '100%'
                }, 'slow');
            }
        });
    });
</script>
</html>
